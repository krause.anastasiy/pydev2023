import cowsay
import asyncio

clients = {}
authorized_clients = {}
available_cow_names = cowsay.list_cows()

def is_authorized_user(me):
    return me.count(':') == 0


async def chat(reader, writer):
    me = "{}:{}".format(*writer.get_extra_info('peername'))
    print(me)
    clients[me] = asyncio.Queue()
    send = asyncio.create_task(reader.readline())
    receive = asyncio.create_task(clients[me].get())
    while not reader.at_eof():
        done, pending = await asyncio.wait([send, receive], return_when=asyncio.FIRST_COMPLETED)
        for q in done:
            if q is send:
                send = asyncio.create_task(reader.readline())
                data = q.result().decode().replace('\n', '').split()

                if len(data):
                    command = data[0]

                    if command == "cows":
                        writer.write(f"Available names:\n{' '.join(available_cow_names)}\n".encode())
                        await writer.drain()

                    elif command == "login":
                        if not is_authorized_user(me):
                            if len(data) != 2:
                                writer.write(f"Please, enter the username from the list of available cows.\n".encode())
                            else:
                                cow = data[1]
                                if cow not in available_cow_names:
                                    writer.write(f"Selected username is already taken.\n".encode())
                                else:
                                    available_cow_names.pop(available_cow_names.index(cow))
                                    authorized_clients[cow] = clients[me]
                                    del clients[me]
                                    me = cow

                                    print(me, "LOGGED IN")
                        else:
                            writer.write(f"You are already logged in.\n".encode())
                        await writer.drain()

                    elif command == "who":
                        writer.write(f"Authorized clients:\n{' '.join(authorized_clients.keys())}\n".encode())
                        await writer.drain()

                    elif command == "say":
                        if is_authorized_user(me):
                            if len(data) < 3:
                                writer.write(f"Please, enter the username and message you want to send.\n".encode())
                            else:
                                username = data[1]
                                message = " ".join(data[2:])

                                client = authorized_clients.get(username)
                                if client:
                                    await client.put(f"{me}: {message}\n")
                                else:
                                    writer.write(f"User {username} is not logged in.\n".encode())

                        else:
                            writer.write(f"Please, login to send messages.\n".encode())
                        await writer.drain()

                    elif command == "yield":
                        if is_authorized_user(me):
                            if len(data) < 2:
                                writer.write(f"Please, enter the message you want to send.\n".encode())
                            else:
                                message = " ".join(data[1:])

                                for out in authorized_clients.values():
                                    if out is not authorized_clients[me]:
                                        await out.put(f"{me}: {message}\n")
                        else:
                            writer.write(f"Please, login to send messages.\n".encode())
                        await writer.drain()

                    elif command == "quit":
                        writer.close()
                        await writer.wait_closed()

                    else:
                        writer.write(f"Unknown command.\nAvailable commands: cows, login, who, say, yield, quit.\n".encode())
                        await writer.drain()

            elif q is receive:
                receive = asyncio.create_task(authorized_clients[me].get())

                if is_authorized_user(me):
                    writer.write(f"{q.result()}\n".encode())
                    await writer.drain()

    send.cancel()
    receive.cancel()
    print(me, "DONE")

    if clients.get(me):
        del clients[me]
    else:
        del authorized_clients[me]
        available_cow_names.append(me)

    writer.close()
    await writer.wait_closed()

async def main():
    server = await asyncio.start_server(chat, '0.0.0.0', 1337)
    async with server:
        await server.serve_forever()

asyncio.run(main())